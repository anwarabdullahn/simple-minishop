import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class Aside extends Component {
    state = {
        isActive: 'is-active'
    }
	render() {
		return (
			<aside className="menu">
				<p className="menu-label">General</p>
				<ul className="menu-list">
					<li>
						<Link to="/" className="is-active">All Items</Link>
					</li>
					<li>
						<Link to="/clothes">Clothes</Link>
					</li>

					<li>
						<Link to="/watches">Watches</Link>
					</li>

					<li>
						<Link to="/shoes">Shoes</Link>
					</li>
				</ul>
			</aside>
		);
	}
}
