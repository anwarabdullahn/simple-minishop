import React from 'react';

const Header = () => {
	return (
		<div>
			<section className="hero is-medium is-primary is-bold">
				<div className="hero-body">
					<div className="container">
						<h1 className="title">Minishop</h1>
						<h2 className="subtitle">Find Your Items Here</h2>
					</div>
				</div>
			</section>
		</div>
	);
};

export default Header;
