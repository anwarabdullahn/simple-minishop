import React from 'react';

const Cards = (props) => {
	const { products } = props;
	const productList = products.map((product, index) => {
		return (
			<div className="column is-one-quarter-desktop is-half-tablet">
				<div className="card" key={index}>
					<header className="card-header">
						<p className="card-header-title">{product.name}</p>
					</header>
					<div className="card-image">
						<figure className="image">
							<img src={product.images} alt="Placeholder image" />
						</figure>
					</div>
					<div className="card-content">
						<div className="content">{product.desc}</div>
						<div className="content">Rp. {product.price}</div>
					</div>
				</div>
				<br />
			</div>
		);
	});

	return <div className="columns is-multiline">{[ productList ]}</div>;
};
export default Cards;
