import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Header from './components/Header';
import Menus from './components/Menus';
import Cards from './components/Cards';
import Aside from './components/Aside';

class App extends Component {
	constructor() {
		super();
		this.state = {
			clothesList: '',
			watchesList: '',
      shoesList:'',
      products: [
        {
          name: `Jas Hitam`,
          price: 420.0,
          desc: `Jas Formal`,
          category: `Baju`,
          images: require('./assets/img/baju3.jpg')
        },
        {
          name: `Jam Tangan Casio`,
          price: 820.0,
          desc: `Jam tangan casio Bagus Untuk Cewek`,
          category: `Jam`,
          images: require('./assets/img/jam2.jpg')
        },
        {
          name: `Baju Kaos Cewek`,
          price: 60.0,
          desc: `Baju Kaos Ini Bagus Untuk Cewek`,
          category: `Baju`,
          images: require('./assets/img/baju6.jpg')
        },
        {
          name: `Jam Tangan BoboBird`,
          price: 220.0,
          desc: `Jam Tangan BoboBird bagus`,
          category: `Jam`,
          images: require('./assets/img/jam1.jpg')
        },

        {
          name: `Sepatu Belang Belang`,
          price: 720.0,
          desc: `Sepatu Belang Belang Ini Bagus Untuk dipake`,
          category: `Sepatu`,
          images: require('./assets/img/sepatu1.jpg')
        },
        {
          name: `Sepatu Converse`,
          price: 620.0,
          desc: `Sepatu Converse warna pink murah`,
          category: `Sepatu`,
          images: require('./assets/img/sepatu2.jpg')
        },
        {
          name: `Baju Raglan Merah`,
          price: 120.0,
          desc: `Baju Raglan Ini Bagus`,
          category: `Baju`,
          images: require('./assets/img/baju1.jpeg')
        },
        {
          name: `Baju Raglan Merah Cewek`,
          price: 120.0,
          desc: `Baju Raglan Ini Bagus Untuk Cewek`,
          category: `Baju`,
          images: require('./assets/img/baju2.jpeg')
        },
        {
          name: `Jam Tangan G-SHOCK`,
          price: 320.0,
          desc: `Jam Tangan G-SHOCK Untuk Semua`,
          category: `Jam`,
          images: require('./assets/img/jam3.jpg')
        },
        {
          name: `Jam Tangan Casio`,
          price: 120.0,
          desc: `Jam Tangan Casio`,
          category: `Jam`,
          images: require('./assets/img/jam4.jpg')
        },
        {
          name: `Jam Tangan Casio Illuminator`,
          price: 820.0,
          desc: `Jam Tangan Casio Illuminator Ini Bagus`,
          category: `Jam`,
          images: require('./assets/img/jam5.jpg')
        },

        {
          name: `Baju Atasan Cewek`,
          price: 90.0,
          desc: `Baju Atasan untuk cewek`,
          category: `Baju`,
          images: require('./assets/img/baju4.jpg')
        },
        {
          name: `Jaket Hoodie Cewek`,
          price: 220.0,
          desc: `Jaket Hoodie Ini Bagus Untuk Cewek`,
          category: `Baju`,
          images: require('./assets/img/baju5.jpg')
        },
        {
          name: `Sepatu Putih`,
          price: 120.0,
          desc: `Sepatu Putih Ini Bagus Untuk Cewek`,
          category: `Sepatu`,
          images: require('./assets/img/sepatu3.jpg')
        }
      ]
    }
  }

  componentDidMount() {
    const clothesList = this.state.products.filter((product) => product.category === `Baju`),
			watchesList = this.state.products.filter((product) => product.category === `Jam`),
      shoesList = this.state.products.filter((product) => product.category === `Sepatu`)

    this.setState({
      clothesList,
      watchesList,
      shoesList
    })
  }

	render() {
		return (
			<Router>
				<div>
					<Header />
					<Menus />
					<div className="container" style={{ marginTop: '50px' }}>
						<div className="columns">
							<div className="column is-4">
								<Aside/>
							</div>
							<div className="column is-8">
								<Route
									path="/" exact
									render={(props) => <Cards {...props} products={this.state.products} />}
								/>
								<Route
									path="/clothes" exact
									render={(props) => <Cards {...props} products={this.state.clothesList} />}
								/>
								<Route
									path="/watches" exact
									render={(props) => <Cards {...props} products={this.state.watchesList} />}
								/>
								<Route
									path="/shoes" exact
									render={(props) => <Cards {...props} products={this.state.shoesList} />}
								/>
							</div>
						</div>
					</div>
				</div>
			</Router>
		);
	}
}

export default App;
